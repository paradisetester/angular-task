<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;
    protected $fillable = [
        'name','dob','contact_number','gender','address','class','status','course_id'
    ];

    public function course()  
    {  
        return $this->hasOne(Course::class, 'id');
    }  
}
