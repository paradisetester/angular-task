<?php

namespace App\Http\Controllers;

use App\Models\Course;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    
    public function index()
    {
        return Course::all();
    }

    
    public function store(Request $request)
    {
        $course = Course::create($request->all());
        return response()->json($course, 201);
    }

    
    public function show(Course $course)
    {
        if(!$course){
            return response()->json(400);
        }
        return $course;
    }


    
    public function update(Request $request, Course $course)
    {
        $course->update($request->all());
        return response()->json($course, 200);
    }

    
    public function destroy(Course $course)
    {
        $course->delete();
        return response()->json(200);
    }
}
