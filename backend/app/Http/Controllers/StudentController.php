<?php

namespace App\Http\Controllers;

use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StudentController extends Controller
{
    
    public function index()
    {
        $student = DB::table('students')
            ->select('students.*', 'courses.course_name')
            ->join('courses', 'students.course_id', '=', 'courses.id')
            ->get();
        return $student;
    }
   
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'dob' => 'required',
            'contact_number' => 'required',
            'gender' => 'required',
            'address' => 'required',
            'class' => 'required',
        ]);
        $student = Student::create($request->all());
        return response()->json($student, 201);
    }

   
    public function show(Student $student)
    {
        $student = DB::table('students')
        ->join('courses', 'students.course_id', '=', 'courses.id')
        ->where('students.id', '=', $student->id)
        ->first();
         return response()->json($student, 201);
    }

    
    public function update(Request $request, Student $student)
    {
        $student->update($request->all());
        return response()->json($student, 200);
    }

    
    public function destroy(Request $request,$id)
    {
        $student = Student::findOrFail($id);
        $student->delete();
        return response()->json(null);  
    }
    public function delete(Request $request,$id)
    {
        $student=Student::where('id',$id)->delete();
        return response()->json($student,200);
    }
}
