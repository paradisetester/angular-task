<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class CourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('courses')->insert([
            [
                'course_name' => 'Test Course 1',
                'course_duration' => '16 Weeks',
            ],
            [
                'course_name' => 'Test Course 2',
                'course_duration' => '4 Weeks',
            ],
            [
                'course_name' => 'Test Course 3',
                'course_duration' => '2 Weeks',
            ],
            [
                'course_name' => 'Test Course 4',
                'course_duration' => '12 Weeks',
            ],
        ]);
    }
}
