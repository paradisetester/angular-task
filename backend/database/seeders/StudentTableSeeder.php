<?php

namespace Database\Seeders;

use App\Models\Student;
use Illuminate\Database\Seeder;

class StudentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Student::truncate();

        $faker = \Faker\Factory::create();
        $int= mt_rand(1262055681,1262055681);
        // And now, let's create a few articles in our database:
        for ($i = 0; $i < 50; $i++) {
            Student::create([
                'name' => $faker->firstName,
                'dob' => date("Y-m-d H:i:s",$int),
                'contact_number' => $faker->tollFreePhoneNumber,
                'gender' => 'male',
                'address' => $faker->paragraph,
                'class' => '5'
            ]);
        }
    }
}
