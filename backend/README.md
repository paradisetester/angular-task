## Clone Project
1. composer install
2. Set db environment details in .env file (DB_DATABASE , DB_USERNAME , DB_PASSWORD).
3. php artisan migrate
## Seeder
Course Seeder
1. php artisan db:seed --class=CourseSeeder