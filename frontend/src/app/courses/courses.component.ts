import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css']
})
export class CoursesComponent implements OnInit {
  baseUrl = environment.baseUrl;
  courses:any;
  constructor(private http:HttpClient) {
    this.http.get(`${this.baseUrl}/courses`).subscribe((response) => 
      {
        console.log(response);
        this.courses = response;
      }
    )
  }

  ngOnInit(): void {
  }

}
