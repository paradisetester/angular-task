import { Component, OnInit } from '@angular/core';
import { HttpClient } from  '@angular/common/http';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-edit-student',
  templateUrl: './edit-student.component.html',
  styleUrls: ['./edit-student.component.css']
})
export class EditStudentComponent implements OnInit {
  baseUrl = environment.baseUrl;
  courses:any;
  student:any;
  updateform = new FormGroup({
    name: new FormControl('', [Validators.required]),
    dob: new FormControl('', [Validators.required]),
    contact_number: new FormControl('', [Validators.required]),
    gender: new FormControl('', [Validators.required]),
    address: new FormControl('', [Validators.required]),
    class: new FormControl('', [Validators.required]),
    course_id: new FormControl('', [Validators.required]),
  });
  constructor(private http: HttpClient, private router:Router, private route: ActivatedRoute) {
    var id = this.route.snapshot.paramMap.get('id');
    this.http.get(`${this.baseUrl}/students/` + id).subscribe((response) => 
      {
        console.log(response);
        this.student = response;
      }
    )
    this.http.get(`${this.baseUrl}/courses`).subscribe((response) => 
      {
        console.log(response);
        this.courses = response;
      }
    )
  }
  getValues(value:string){
    if(this.updateform.invalid){
      alert('Please Fill Data');
    }else{
      var headers = {
        "headers" :{
          'Content-Type':'application/json'
        }
      };
      var data = value;
      this.http.put(`${this.baseUrl}/students/` + this.route.snapshot.paramMap.get('id'), data, headers ).subscribe((response) => 
        {
          if(response){
            alert('Student Updated');
            this.router.navigate(['/students']);
          }
        }
      )
    }
    
  } 
  ngOnInit(): void {
  }

}
