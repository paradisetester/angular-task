import { Component, OnInit } from '@angular/core';
import { HttpClient } from  '@angular/common/http';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-add-students',
  templateUrl: './add-students.component.html',
  styleUrls: ['./add-students.component.css']
})
export class AddStudentsComponent implements OnInit {
  baseUrl = environment.baseUrl;
  val:any;
  courses:any;
  form = new FormGroup({
    name: new FormControl('', [Validators.required]),
    dob: new FormControl('', [Validators.required]),
    contact_number: new FormControl('', [Validators.required]),
    gender: new FormControl('', [Validators.required]),
    address: new FormControl('', [Validators.required]),
    class: new FormControl('', [Validators.required]),
    course_id: new FormControl('', [Validators.required]),
  });
  constructor(private http: HttpClient, private router:Router) {
    
    this.http.get(`${this.baseUrl}/courses`).subscribe((response) => 
      {
        console.log(response);
        this.courses = response;
      }
    )
  }
  getValues(value:string){
    if(this.form.invalid){
      alert('Please Fill Data');
    }else{
      var headers = {
        "headers" :{
          'Content-Type':'application/json'
        }
      };
      var data = value;
      this.http.post(`${this.baseUrl}/students`, data, headers ).subscribe((response) => 
        {
          if(response){
            alert('Student Added');
            this.router.navigate(['/students']);
          }
        }
      )
    }
    
  } 
  ngOnInit(): void {
  }

}
