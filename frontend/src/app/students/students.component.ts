import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from  '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {
  baseUrl = environment.baseUrl;
  showModalBox: boolean = false;
  showModalEdit: boolean = false;
  sayHelloWorld: boolean = false;
  students:any;
  showModal: boolean = false;
  courses:any;
  content: any;
  title: any;
  heading:any;
  id:any;
  editId:any;
  contact_no:any;
  dob:any;
  gender:any;
  address:any;
  class:any;
  course:any;
  
  constructor(private http: HttpClient, private router:Router) {
    
      this.http.get(`${this.baseUrl}/students`).subscribe((response) => 
      {
        console.log(response);
        this.students = response;
      }
    )
    this.http.get(`${this.baseUrl}/courses`).subscribe((response) => 
      {
        console.log(response);
        this.courses = response;
      }
    )
  }
  myClickFunction(id:string){
    this.showModal = true; // Show-Hide Modal Check
    this.http.get(`${this.baseUrl}/students/` + id).subscribe((response) => 
      {
        var obj = JSON.stringify(response);  
        var data = JSON.parse(obj);
          this.heading = data.name;
          this.dob = data.dob;
          this.contact_no = data.contact_number;
          this.gender = data.gender;
          this.address = data.address;
          this.class = data.class;
          this.course = data.course_name;
      }
    )
  }
  deleteFunction(id:string){
    if(confirm("Are you sure to delete")) {
      this.http.delete(`${this.baseUrl}/students/`+id).subscribe((response) => 
      {
        this.router.navigate(['/']);
      }
    )
    }
  }
  hide(){
    this.showModal = false;
  }
  close(){
    this.showModalEdit = false;
  }
  ngOnInit(): void {
  }
  

}
