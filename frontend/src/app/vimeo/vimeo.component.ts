import { Component, OnInit } from '@angular/core';
import Player from '@vimeo/player';
@Component({
  selector: 'app-vimeo',
  templateUrl: './vimeo.component.html',
  styleUrls: ['./vimeo.component.css']
})
export class VimeoComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    const options = {
      id: 59777392,
      width: 640,
      loop: true
  };
  const madeInNy = new Player('playerContainer', options);
  }

}
